package dao;

import connection.ConnectionDatabase;
import model.Musei;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class MuseiDaoImpl implements MuseiDao {

    private Connection connection;

    private static final String CREATE_MUSEUM = "INSERT INTO musei VALUES(?, ?, ?)";

    public static final String SELECT_ALL_QUERY = "SELECT * FROM musei";

    private static final String FIND_BY_ID = "SELECT * FROM musei WHERE musei_id = ?";

    private static final String UPDATE_CITY_BY_ID = "UPDATE musei m SET m.nome_m = ?, " +
            "m.citta = ? WHERE m.musei_id = ? ";

    private static final String DELETE_MUSEUM = "DELETE musei WHERE musei_id = ?";

    @Override
    public boolean create(Musei musei) {

        PreparedStatement statement = null;

        try {
            connection = ConnectionDatabase.getConnection();

            if (connection != null) {

                statement = connection.prepareStatement(CREATE_MUSEUM);

                statement.setLong(1, musei.getMuseiId());
                statement.setString(2, musei.getNomeM());
                statement.setString(3, musei.getCitta());

                statement.executeQuery();
            }
            return true;
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return false;
    }

    @Override
    public List<Musei> getAllMusei() {

        List<Musei> museiList = new ArrayList<>();
        Musei musei = null;
        PreparedStatement statement = null;
        ResultSet set = null;
        try {
            connection = ConnectionDatabase.getConnection();
            if (connection != null) {
                statement = connection.prepareStatement(SELECT_ALL_QUERY);
                set = statement.executeQuery();
                while (set.next()) {
                    musei = new Musei();

                    musei.setMuseiId(set.getLong(1));
                    musei.setNomeM(set.getString(2));
                    musei.setCitta(set.getString(3));

                    museiList.add(musei);
                }
            }

        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return museiList;
    }

    @Override
    public Musei findById(long id) {

        Musei musei = null;
        PreparedStatement statement = null;
        ResultSet set = null;
        try {
            connection = ConnectionDatabase.getConnection();
            if (connection != null) {
                statement = connection.prepareStatement(FIND_BY_ID);
                statement.setLong(1, id);

                set = statement.executeQuery();

                if (set.next()) {
                    musei = new Musei();

                    musei.setMuseiId(id);
                    musei.setNomeM(set.getString(2));
                    musei.setCitta(set.getString(3));

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
        return musei;
    }

    @Override
    public void updateMuseum(Musei musei) {
        PreparedStatement statement = null;
        ResultSet set = null;
        try {
            connection = ConnectionDatabase.getConnection();
            if (connection != null) {
                statement = connection.prepareStatement(UPDATE_CITY_BY_ID);

                statement.setString(1, musei.getNomeM());
                statement.setString(2, musei.getCitta());
                statement.setLong(3, musei.getMuseiId());

                set = statement.executeQuery();
                if (set.next()) {
                    System.out.println("Updated");
                }
            }


        } catch (SQLException e) {
            System.err.println("Failed");
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public void deleteMuseum(Musei musei) {

        PreparedStatement statement = null;

        try {

            connection = ConnectionDatabase.getConnection();
            if (connection != null) {
                statement = connection.prepareStatement(DELETE_MUSEUM);
                statement.setLong(1, musei.getMuseiId());

                statement.executeQuery();
                System.out.println("Deleted");
            }

        } catch (SQLException e) {
            System.err.println("Failed");
            e.printStackTrace();
        } finally {
            if (statement != null) {
                try {
                    connection.close();
                } catch (SQLException e) {
                    e.printStackTrace();
                }
            }
        }


    }
}
